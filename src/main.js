import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import {routes} from './routes';
import VueResource from 'vue-resource'
import Simplert from 'vue2-simplert'

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Simplert);
Vue.use(require('vue-chartist'))
Vue.use(require('vue-easeljs'));

const router = new VueRouter({
    routes,
    mode : 'history',
});

new Vue({
    el: '#app',
    router,
    render: h => h(App)
})
