import Admin from './components/Admin.vue'
import Grid from './components/Grid.vue'

export const routes = [
    {path : '/', component : Grid},
    {path : '/admin', component : Admin},
];